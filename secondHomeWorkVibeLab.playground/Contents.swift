import UIKit

//Количество дней в месяцах
let arrayWithDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

//Название месяцев
let arrayWithMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

//Вывод количества дней в каждом месяце без имён
for i in 0...11 {
    print(arrayWithDays[i])
}

print("\n----------------------------------------------\n")

//Количество - дни
for i in 0...11 {
    print(arrayWithMonths[i], "===", arrayWithDays[i])
}

print("\n----------------------------------------------\n")

//Массив Тюплов

let arrayWithTuples = [("January", 31), ("February", 29), ("March", 31), ("April", 30), ("May", 31), ("June", 30), ("July", 31), ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]

for i in 0...11 {
    print(arrayWithTuples[i])
}

print("\n----------------------------------------------\n")


//Дни в обратном порядке через массив Тюплов
for i in 0...11 {
    print(arrayWithTuples[i].0, arrayWithTuples[-i+11].1)
    
}

print("\n----------------------------------------------\n")

//Функция считает сколько дней было от начала года до выбранной даты
func howManyDaysBehind(with month: String, day: Int) {
    var sum = 0
    for i in 0...11 {
        if arrayWithTuples[i].0 == month {
            print(sum+day)
            
            return
        }
        sum += arrayWithTuples[i].1
    }
}

howManyDaysBehind(with: "December", day: 31)
